const Router = require('express');
const router = new Router();
const UserController = require("../controllers/user_controller")

router.post("/login", UserController.loginUser);
router.post("/registration", UserController.createUser);

module.exports = router