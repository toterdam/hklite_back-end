const db = require('../db');
const bcrypt = require('bcrypt');
const config = require('../config');
const jwt = require('jsonwebtoken');

class UserController{
    async createUser(req, res) {
        const {login, email, pass, role} = req.body;
        const checkEmailExists = db.module.query('SELECT * from users where email = $1',[email])
        if (checkEmailExists.rowCount > 0){
            res.status(401).json({"status":"failed", "details":"email already registered"});
        }
        const newUser = await db.module.query('INSERT INTO users (login, email, pass, role) values ($1, $2, $3, $4) RETURNING *', [login, email, bcrypt.hashSync(pass, 8), role]);
        let token = jwt.sign(
            { id: newUser.id },
                config.secret,
            { expiresIn: 86400} // истекает через 24 часа
          );
          res.status(200).send({
            auth: true,
            token: token,
            user: newUser
          });
    }
    async loginUser(req,res){
        const {email, pass} = req.body;
        const loginUser = await db.module.query('SELECT * from users where email = $1',[email]);
        if (loginUser.rowCount === 0){
            res.status(401).json({"status":"failed", "details":"user not found"});
        }
        else{
            let passwordIsValid = bcrypt.compareSync(pass, loginUser.rows[0]["pass"]);
            if (!passwordIsValid)
            {
                return res.status(401).send(
                    {
                        auth: false,
                        token: null
                    }
                )
            }
            let token = jwt.sign(
                { id: loginUser.rows[0]["id"] },
                    config.secret,
                { expiresIn: 86400 } // истекает через 24 часа
            );
            res.status(200).send(
                {
                    auth: true,
                    token: token,
                    user: loginUser
                }
            )
        }
     }
}

module.exports = new UserController();